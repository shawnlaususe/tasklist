$(document).ready(function() {
	$('#btnAddTask').click(function(evt) {
		evt.preventDefault();
		$('#taskCreation').removeClass('not');
		console.log($(evt.target).html());
	});
	
	$('tbody tr').click(function(evt) {
		console.log($(evt.target).html());
		$(evt.target).closest('td').siblings().andSelf().toggleClass('rowHighlight');
	});
});

/* This custom plugin adds two new features to jQuery:
	- Serialize the values on a form into an object
	- Deserialize an object onto a form
*/
(function($) {
	$.fn.extend({
		toObject:function() {
			var result = {}
			/* Turn the form into an array of objects.  Populate your empty result
			object with the key/value pairs from your form array*/ 
			$.each(this.serializeArray(), function(i,v) {
				result[v.name] = v.value;
			});
			return result;
		},
		/* Instead of turning into an array, extract the key value pairs from an 
		existing form object and assign them to the input fields on the page */
		fromObject:function(obj) {
			$.each(this.find(':input'), function(i,v) {
				var name = $(v).attr('name');
				if (obj[name]) {
					$(v).val(obj[name]);
				} else {
					$(v).val('');
				}
			});
		}
	});
})(jQuery);

/*****************************************************
Call functions from existing plugins.  These plugins
automatically add new features to jQuery, just as our
own custom plugin add new features to jQuery
******************************************************/
/* jQuery Template - generates HTML from a template.  You will
create a block of HTML in the view, except that will contain placeholders
for values.  Then you will use the jQuery below to manipulate those placeholders */

